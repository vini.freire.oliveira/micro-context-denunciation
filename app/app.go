package main

import (
	"fmt"
	"time"
	"os"

	"gopkg.in/robfig/cron.v2"
	"github.com/streadway/amqp"

	S "denunciation/app/services"
)

func main() {

	// host := "amqp://" + os.Getenv("RABBITMQ_USER") + ":" + os.Getenv("RABBITMQ_USER") + "@" + os.Getenv("RABBITMQ_HOST") + ":" + os.Getenv("RABBITMQ_PORT") + "/"

	conn, err := amqp.Dial("amqp://guest:guest@rabbitmq:5672/")

	if err != nil {
		fmt.Println(os.Getenv("RABBITMQ_USER"))
		fmt.Println(err)
		panic(1)
	}
	defer conn.Close()

	fmt.Println("Successfully Connected to our RabbitMQ Instance")

	c := cron.New()

	c.AddFunc("@every 0h1m0s", S.Consumer(conn))

	c.Start()

	for{
		fmt.Println("version ==>", version)

		time.Sleep(5 * time.Minute)
	}

}