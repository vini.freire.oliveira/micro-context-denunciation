package operations

import (
	"fmt"
	"log"
	"encoding/json"

	mgo "gopkg.in/mgo.v2"
	bson "gopkg.in/mgo.v2/bson"
)

type Message struct {
	CorrelationId string
	Action string
	ContentType string
	Message Denunciation
	When string
}

type Denunciation struct {
	Description string
	Longitude string
	Latitude string
	Status int
	UserId string
	Protocol string
}

func Operation(msg []byte) {
	session, err := mgo.Dial("database_context_denunciation")

	if err != nil {
		log.Fatal(err)
		panic(err)
	}

	defer session.Close()

	c := session.DB("context_denunciation").C("denunciations")

	var message Message
	if err := json.Unmarshal(msg, &message); err != nil {
			panic(err)
	}

	result := Denunciation{}

	switch message.Action {
	case "create":
		message.Message.Status = 0
		err = c.Insert(message.Message)
	case "update":
		err = c.Update(bson.M{"protocol": message.Message.Protocol}, message.Message)
	case "destroy":
		err = c.Remove(bson.M{"protocol": message.Message.Protocol})
	case "find":
		err = c.Find(bson.M{"protocol": message.Message.Protocol}).One(&result)
	default:
		fmt.Println("invalid operation")
	}

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("result:", result)
}