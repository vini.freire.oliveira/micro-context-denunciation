module denunciation/app

go 1.15

require (
	github.com/streadway/amqp v1.0.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/robfig/cron.v2 v2.0.0-20150107220207-be2e0b0deed5
)
