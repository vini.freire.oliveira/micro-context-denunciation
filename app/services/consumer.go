package services

import (
    "fmt"

		"github.com/streadway/amqp"

		D "denunciation/app/operations"
)

func Consumer(conn *amqp.Connection) func() {

	fmt.Println("Consumer has called")

	ch, err := conn.Channel()
	if err != nil {
		fmt.Println(err)
	}
	defer ch.Close()

	if err != nil {
	fmt.Println(err)
	}

	msgs, err := ch.Consume(
		"context-denunciation",
		"",
		true,
		false,
		false,
		false,
		nil,
	)

	forever := make(chan bool)
		go func() {
			for d := range msgs {
				fmt.Println("Recieved and preparing to operate the Message")
				D.Operation(d.Body)
			}
		}()

		fmt.Println("Successfully Connected to our RabbitMQ Instance")
		fmt.Println(" [*] - Waiting for messages")
	<-forever
    
	return func() {
		fmt.Println(msgs)
	}
}
